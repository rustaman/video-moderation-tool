import { useEffect, useState } from 'react';
import VideoCard from '../../components/Video/VideoCard';
import { VideoType } from '../../utils/types';
import ReportModal from '../../components/Report/ReportModal';
import firebaseMethods from '../../utils/firebase';
import toast from 'react-hot-toast';

const HomePage = () => {
    const [page, setPage] = useState(1);
    const [videos, setVideos] = useState<VideoType[]>([]);
    const [index, setIndex] = useState(0);
    const [displayModal, setDisplayModal] = useState(false);
    const currentVideo = videos.length > 0 ? videos[index] : null;

    useEffect(() => {
        fetch(
            `https://api.dailymotion.com/videos?fields=owner%2Ctitle%2Cembed_url&sort=recent&limit=10&page=${page}`
        )
            .then((response) => {
                return response.json();
            })
            .then((jsonResponse) => {
                setVideos(jsonResponse.list);
            });
    }, [page]);

    const next = () => {
        setIndex((currentIndex) => {
            if (currentIndex == videos.length - 1) {
                setPage((currentPage) => currentPage + 1);
                return 0;
            } else {
                return currentIndex + 1;
            }
        });
    };

    const legit = () => {
        if (currentVideo) {
            firebaseMethods.addOrUpdateVideo(currentVideo.embed_url, true, '');
        }
        toast.success('Marked as legit');
        next();
    };

    const report = (reason: string) => {
        if (currentVideo) {
            firebaseMethods.addOrUpdateVideo(
                currentVideo.embed_url,
                false,
                reason
            );
        }
        hideModal();
        toast.success('Sucessfully reported');
        next();
    };

    const showModal = () => {
        setDisplayModal(true);
    };

    const hideModal = () => {
        setDisplayModal(false);
    };

    return (
        <div className="mx-auto mt-10 ">
            {displayModal && (
                <ReportModal discard={hideModal} report={report} />
            )}

            {currentVideo && (
                <VideoCard
                    key={index}
                    title={currentVideo.title}
                    owner={currentVideo.owner}
                    embed_url={currentVideo.embed_url}
                    legit={legit}
                    report={showModal}
                />
            )}
        </div>
    );
};

export default HomePage;
