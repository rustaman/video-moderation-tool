import { Link, useRouteError } from 'react-router-dom'

type ErrorsData = {
    status: number
    statusText: string
}

const ErrorPage = () => {
    const errorData = useRouteError() as ErrorsData
    return (
        <div className="flex flex-col items-center gap-4 p-8">
            <h1 className="text-3xl font-bold">Oups..</h1>
            {errorData && (
                <div className="flex gap-2">
                    <span className="text-lg">{errorData.status}</span>
                    <p className="text-lg">{errorData.statusText}</p>
                </div>
            )}
            {!errorData.statusText && (
                <p className="text-lg">Something wrong happened</p>
            )}
            <Link
                to="/"
                className="rounded-full bg-slate-900 px-6 py-2 font-bold text-white"
            >
                Go Home
            </Link>
        </div>
    )
}

export default ErrorPage
