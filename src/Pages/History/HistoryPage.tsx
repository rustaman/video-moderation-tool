import { useEffect, useState } from 'react';
import firebaseMethods from '../../utils/firebase';

const HistoryPage = () => {
    const [verdicts, setVerdicts] = useState<Verdict>();

    useEffect(() => {
        firebaseMethods.getAllVdeos().then((videos) => setVerdicts(videos));
    }, []);

    return <div></div>;
};

export default HistoryPage;
