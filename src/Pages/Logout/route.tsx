import { LoaderFunction, redirect } from 'react-router-dom';
import firebaseMethods from '../../utils/firebase';

export const loader: LoaderFunction = async () => {
    await firebaseMethods.signOut();
    return redirect('/login');
};
