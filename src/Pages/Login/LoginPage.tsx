import { Form, useActionData } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { ArrowPathIcon } from '@heroicons/react/24/outline';

const LoginPage = () => {
    const data = useActionData() as { error?: string };
    const [isLoging, setIsLoging] = useState(false);

    useEffect(() => {
        setIsLoging(false);
    }, [data]);

    return (
        <div className="flex flex-col items-center">
            <h1 className="mb-4 text-2xl font-semibold">Log in</h1>
            {data && data.error && <p className="text-red-700">{data.error}</p>}
            <Form
                action="/login"
                method="POST"
                onSubmit={() => setIsLoging(true)}
            >
                <div className="mb-4">
                    <label className="block font-medium" htmlFor="email">
                        Email
                    </label>
                    <input type="email" name="email" id="email" required />
                </div>
                <div className="mb-4">
                    <label className="block font-medium" htmlFor="password">
                        Password
                    </label>
                    <input
                        type="password"
                        name="password"
                        id="password"
                        required
                    />
                </div>
                <button
                    type="submit"
                    className="btn w-full"
                    disabled={isLoging}
                >
                    {isLoging ? (
                        <ArrowPathIcon className="mx-auto block animate-spin" />
                    ) : (
                        'Log in'
                    )}
                </button>
            </Form>
        </div>
    );
};

export default LoginPage;
