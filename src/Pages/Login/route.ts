import { ActionFunction, LoaderFunction, redirect } from 'react-router-dom';
import fireBaseMethods from '../../utils/firebase';

export const action: ActionFunction = async ({ request }) => {
    const formData = await request.formData();
    const email = formData.get('email') as string;
    const password = formData.get('password') as string;

    if (email && password) {
        try {
            await fireBaseMethods.signIn(email, password);
            return redirect('/');
        } catch (error) {
            return { error: 'Invalid email or password' };
        }
    }

    return null;
};

export const loader: LoaderFunction = async () => {
    const user = await fireBaseMethods.getUser();
    if (user) {
        return redirect('/');
    } else {
        return null;
    }
};
