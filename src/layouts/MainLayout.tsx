import { Outlet, useNavigate, useLocation } from 'react-router-dom';
import NavBar from '../components/Navigation/NavBar';
import firebaseMethods from '../utils/firebase';
import { User } from 'firebase/auth';
import { useEffect, useState } from 'react';
import { Toaster } from 'react-hot-toast';

const MainLayout = () => {
    const navigate = useNavigate();
    const location = useLocation().pathname;
    const [user, setUser] = useState<User | null>();

    useEffect(() => {
        firebaseMethods.getUser().then((user) => setUser(user));
    }, [location]);

    useEffect(() => {
        if (!user) {
            navigate('/login');
        }
    }, [navigate, user]);

    return (
        <div className="h-screen overflow-y-scroll">
            {user && <NavBar user={user} />}
            <div className="py-4">
                <Outlet />
            </div>
            <Toaster
                position="bottom-center"
                toastOptions={{
                    style: {
                        width: '400px',
                        height: '50px',
                        fontSize: '24px',
                        fontWeight: 'bold',
                    },
                }}
            />
        </div>
    );
};

export default MainLayout;
