import { useState } from 'react';
import { createPortal } from 'react-dom';

type ModalProps = {
    discard: () => void;
    report: (reason: string) => void;
};

const Modal: React.FC<ModalProps> = ({ discard, report }) => {
    const [reason, setReason] = useState<string>('');
    return (
        <form
            onSubmit={(event) => {
                event.preventDefault();
                if (reason && reason.length > 0) {
                    report(reason);
                }
            }}
            className="absolute left-2/4 top-2/4 z-20 flex -translate-x-2/4 -translate-y-2/4 flex-col gap-4 rounded-md bg-white p-4 shadow-lg"
        >
            <label className="text-2xl font-medium" htmlFor="reason">
                Report reason
            </label>
            <textarea
                name="reason"
                id="reason"
                cols={40}
                rows={10}
                onChange={(event) => {
                    setReason(event.target.value);
                }}
                className="resize-none rounded-md border-2 border-solid border-gray-300 p-2"
            ></textarea>
            <div className="mt-2 flex justify-end gap-3">
                <button type="button" className="btn" onClick={() => discard()}>
                    Cancel
                </button>
                <button type="submit" className="btn bg-red-700 font-medium">
                    Confirm Report
                </button>
            </div>
        </form>
    );
};

const ReportModal: React.FC<ModalProps> = (props) => {
    return createPortal(
        <>
            <div
                className="fixed z-10 h-screen w-screen bg-transparent-white"
                onClick={() => props.discard()}
            ></div>
            <Modal {...props} />
        </>,
        document.getElementById('report-modal') as Element
    );
};

export default ReportModal;
