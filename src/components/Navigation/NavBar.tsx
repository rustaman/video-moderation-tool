import { NavLink } from 'react-router-dom';
import {
    PlayCircleIcon,
    QueueListIcon,
    ArrowLeftOnRectangleIcon,
} from '@heroicons/react/24/outline';
import { User } from 'firebase/auth';

type Props = {
    user: User | null;
};

const NavBar: React.FC<Props> = ({ user }) => {
    return (
        <ul className="nav">
            <li>
                <NavLink to="/">
                    <PlayCircleIcon />
                    <span>Moderation</span>
                </NavLink>
            </li>
            <li>
                <NavLink to="history">
                    <QueueListIcon />
                    <span>History</span>
                </NavLink>
            </li>
            {user && (
                <li>
                    <NavLink to="logout">
                        <ArrowLeftOnRectangleIcon />
                        <span>Logout</span>
                    </NavLink>
                </li>
            )}
        </ul>
    );
};

export default NavBar;
