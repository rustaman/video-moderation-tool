import { VideoType } from '../../utils/types';
import { motion } from 'framer-motion';

const TITLE_TEXT_SIZE_LIMIT = 50;
const SUBTITLE_TEXT_SIZE_LIMIT = 40;

type VerdictProps = {
    legit: () => void;
    report: () => void;
};

const VideoCard: React.FC<VideoType & VerdictProps> = ({
    owner,
    title,
    embed_url: url,
    legit,
    report,
}) => {
    const displayedTitle =
        title.length > TITLE_TEXT_SIZE_LIMIT
            ? `${title.slice(0, TITLE_TEXT_SIZE_LIMIT)}...`
            : title;

    const diplayedOwner =
        owner.length > SUBTITLE_TEXT_SIZE_LIMIT
            ? `${owner.slice(0, SUBTITLE_TEXT_SIZE_LIMIT)}...`
            : owner;

    return (
        <motion.div
            className="mx-auto flex w-fit flex-col"
            initial={{
                y: '100%',
            }}
            animate={{
                y: '0',
            }}
        >
            <iframe
                className="rounded-lg bg-black"
                height="720"
                width="1080"
                src={url}
            ></iframe>
            <div className="mt-2 flex justify-between">
                <div>
                    <h1 className="text-xl font-medium capitalize">
                        {displayedTitle}
                    </h1>
                    <h3 className="text-md font-medium">
                        by{' '}
                        <span className="italic text-gray-800">
                            {diplayedOwner}
                        </span>
                    </h3>
                </div>
                <div className="flex gap-2">
                    <button
                        className="btn bg-green-600 font-medium text-white"
                        onClick={() => legit()}
                    >
                        Legit
                    </button>
                    <button
                        className="btn bg-red-600 font-medium text-white"
                        onClick={() => report()}
                    >
                        Report
                    </button>
                </div>
            </div>
        </motion.div>
    );
};

export default VideoCard;
