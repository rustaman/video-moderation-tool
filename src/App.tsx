import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import MainLayout from './layouts/MainLayout';
import ErrorPage from './Pages/Error/ErrorPage';
import HomePage from './Pages/Home/HomePage';
import LoginPage from './Pages/Login/LoginPage';
import HistoryPage from './Pages/History/HistoryPage';

import {
    action as loginAction,
    loader as loginLoader,
} from './Pages/Login/route';

import { loader as logoutLoader } from './Pages/Logout/route';

const router = createBrowserRouter([
    {
        path: '/',
        element: <MainLayout />,
        errorElement: <ErrorPage />,
        children: [
            {
                index: true,
                element: <HomePage />,
            },
            {
                path: 'history',
                element: <HistoryPage />,
            },
            {
                path: '/login',
                element: <LoginPage />,
                action: loginAction,
                loader: loginLoader,
            },
            {
                path: '/logout',
                loader: logoutLoader,
            },
        ],
    },
]);

function App() {
    return <RouterProvider router={router} />;
}

export default App;
