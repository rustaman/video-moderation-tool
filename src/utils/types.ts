export type VideoType = {
    owner: string;
    title: string;
    embed_url: string;
};

export type Verdict = {
    url: string;
    legit: boolean;
    reason: string;
};
