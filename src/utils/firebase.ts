import { initializeApp } from 'firebase/app';
import {
    getAuth,
    signOut,
    signInWithEmailAndPassword,
    setPersistence,
    browserLocalPersistence,
} from 'firebase/auth';

import {
    getFirestore,
    collection,
    addDoc,
    query,
    where,
    getDocs,
    updateDoc,
} from 'firebase/firestore';
import { Verdict } from './types';

const firebaseConfig = {
    apiKey: 'AIzaSyBRIDaYCY5oOFpbchNXEuLWpgU9I81-0ww',
    authDomain: 'video-moderation-tool.firebaseapp.com',
    projectId: 'video-moderation-tool',
    storageBucket: 'video-moderation-tool.appspot.com',
    messagingSenderId: '953105392317',
    appId: '1:953105392317:web:ed05e4e62aca1139724dd4',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize firestore
const db = getFirestore(app);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);

setPersistence(auth, browserLocalPersistence);

const videosRef = collection(db, 'videos');

const methods = {
    signIn: async (email: string, password: string) => {
        await signInWithEmailAndPassword(auth, email, password);
    },
    signOut: async () => {
        await signOut(auth);
    },
    getUser: async () => {
        await auth.authStateReady();
        return auth.currentUser;
    },
    addOrUpdateVideo: async (
        url: string,
        legit: boolean,
        reason: string | null
    ) => {
        const q = query(videosRef, where('url', '==', url));
        const querySnapshot = await getDocs(q);
        if (querySnapshot.empty) {
            return addDoc(videosRef, {
                url,
                legit,
                reason: reason ?? '',
            });
        } else {
            const videoDocRef = querySnapshot.docs[0].ref;
            return updateDoc(videoDocRef, {
                legit,
                reason: reason ?? '',
            });
        }
    },
    getAllVdeos: async () => {
        return getDocs(videosRef).then((result) => {
            return result.docs.map((doc) => doc.data() as Verdict);
        });
    },
};

export default methods;
