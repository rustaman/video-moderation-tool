/** @type {import('tailwindcss').Config} */

// eslint-disable-next-line no-undef
module.exports = {
    content: ['./src/**/*.{ts,tsx}'],
    theme: {
        extend: {
            colors: {
                'transparent-white': 'rgba(255, 255, 255, 0.5)',
            },
        },
    },
    plugins: [require('@tailwindcss/forms')],
};
